package com.studentmanagement.example.services;

import com.studentmanagement.example.models.Grade;
import com.studentmanagement.example.repositories.GradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Luca Andrei on 4/9/2017.
 */
@Service
public class GradeServiceImpl implements GradeService {
    @Autowired
    private GradeRepository repository;

    @Override
    public Grade save(Grade entity) {
        return repository.save(entity);
    }

    @Override
    public List<Grade> getAll() {
        return repository.findAll();
    }

    @Override
    public Grade getById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }
}
