package com.studentmanagement.example.services;

import com.studentmanagement.example.models.Grade;

/**
 * Created by Luca Andrei on 4/9/2017.
 */
public interface GradeService extends CrudService<Grade>{

}
