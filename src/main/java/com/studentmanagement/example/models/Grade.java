package com.studentmanagement.example.models;

import org.hibernate.annotations.Parent;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "grade")
public class Grade implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "targetStudent")
    private Student targetStudent;

    @Column
    private Long grade;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGrade() {
        return grade;
    }

    public void setGrade(Long grade) {
        if (grade <=10 && grade > 0) {
            this.grade = grade;
        }
    }

    public Student getTargetStudent() {
        return targetStudent;
    }

    public void setTargetStudent(Student targetStudent) {
        this.targetStudent = targetStudent;
    }
}

/**
 * Exemplu JSON
 * {
     "targetStudent" : {"id": "1", "firstName": "Florin1", "lastName": "Olariu1" },
     "grade" : 10
   }
 */