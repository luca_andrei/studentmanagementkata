package com.studentmanagement.example.controllers;

import com.studentmanagement.example.models.Grade;
import com.studentmanagement.example.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Luca Andrei on 4/9/2017.
 */
@RestController
@RequestMapping("/v1/grades")
public class GradeController {
    @Autowired
    private GradeService gradeService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Grade>> getAll() {
        List<Grade> gradeList;
        gradeList = this.gradeService.getAll();

        if (gradeList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(gradeList, HttpStatus.FOUND);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Grade> addGrade(@RequestBody Grade grade) {
        if (grade.getGrade() > 10 || grade.getGrade() < 1) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (gradeService.save(grade) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(grade, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Grade> getByID(@PathVariable("id") Long id) {
        Grade grade;
        grade = this.gradeService.getById(id);

        if (grade == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return  new ResponseEntity<>(grade, HttpStatus.FOUND);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Grade> delete(@PathVariable("id") Long id) {
        Grade student = this.gradeService.getById(id);
        if (student == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        this.gradeService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
