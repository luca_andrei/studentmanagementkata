package com.studentmanagement.example.repositories;

import com.studentmanagement.example.models.Grade;
import com.studentmanagement.example.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Luca Andrei on 4/9/2017.
 */
public interface GradeRepository extends JpaRepository<Grade, Long> {
}
